import rasterio
import rasterio.features
import pandas as pd
import numpy as np
import os

from pyproj import Proj

# create a pyproj object for indiana zone 16K
testProy = Proj("+proj=utm +zone=16K, +ellps=WGS84 +datum=WGS84 +units=m +no_defs")

# get the file stored on that path
path = './AUX_DATA/'
jp2s_dir = os.listdir(path)

# images extension if there are other files 
file_ext = '.jp2'

# filter the images if there are other files
jp2s = [x for x in jp2s_dir if file_ext in x ]

# create an empty dataframe to store the geom information
coor_DF = pd.DataFrame(columns = ['imagen_name', 'point_a', 'point_b', 'lat', 'lon'])

for image in jp2s:
    
    print('Processing: ' + image)
    
    image_path = path + image

    with rasterio.open(image_path) as dataset:

        # Read the dataset's valid data mask as a ndarray.
        mask = dataset.dataset_mask()

        # Extract feature shapes and values from the array.
        for geom, val in rasterio.features.shapes(
                mask, transform=dataset.transform):

            # transform the geom values into a numpy array
            geom_array = np.array(geom['coordinates'], dtype=object).tolist()

            # iterate the array to transform each one of the five points
            for i in range(0,len(geom_array[0])):

                # transform the values into UTM coordinates using the testProy declared in the first line
                lon, lat = testProy(geom_array[0][i][0], geom_array[0][i][1], inverse=True)

                # add the values to the data frame
                coor_DF = coor_DF.append({'imagen_name' : image, 'point_a' : geom_array[0][i][0], 
                                          'point_b' : geom_array[0][i][1], 'lat' : lat, 'lon' : lon}, ignore_index = True)

                
coor_DF.to_csv('coor_DF.csv')
print(coor_DF)
